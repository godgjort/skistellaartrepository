package tests.uiTests.verifyLogin;

import core.helperClasses.AppLauncher;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestLoginActivity extends AppLauncher {


    AndroidDriver<AndroidElement> androidDriver;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        // Instantiating an object of 'AndroidCapabilities()' class.
        androidDriver = AndroidCapabilities();
    }

    @BeforeMethod
    public void beforeMethod() throws MalformedURLException {
        androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public void afterTests() {
        androidDriver.quit();
    }

    @Test
    public void TestLoginActivityElements() {

        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        System.out.println("Activity Name for LoginScreen" + androidDriver.currentActivity());

        androidDriver.findElementById("com.skistar.SKISTAR1:id/login_button").isDisplayed();

        // Verify that all the available attributes for 'LoginActivity' are present.
        MobileElement imageButtonX = androidDriver.findElementByXPath("//android.widget.ImageButton" +
                "[@content-desc = 'Navigate up' and @instance = '0']");

        MobileElement imageViewMySkiStar = androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '0' and @resource-id = 'com.skistar.SKISTAR1:id/placeholder']");

        MobileElement editTextEmail = androidDriver.findElementByXPath("//android.widget.EditText" +
                "[@text = 'Email' and @resource-id = 'com.skistar.SKISTAR1:id/email_input_field']");

        MobileElement editTextPwd = androidDriver.findElementByXPath("//android.widget.EditText" +
                "[@text = 'Password' and @resource-id = 'com.skistar.SKISTAR1:id/password_input_field']");

        // Special case - will  only be displayed in case of Credentials/NoAccount Error
        // MobileElement textViewInputErr = androidDriver.findElementByXPath("//android.widget.TextView" +
        //        "[@text = 'Incorrect password or there is no such user.' and @resource-id = 'com.skistar.SKISTAR1:id/textinput_error']");

        MobileElement buttonLogIn = androidDriver.findElementByXPath("//android.widget.Button" +
                "[@text = 'Log In' and @resource-id = 'com.skistar.SKISTAR1:id/login_button']");

        MobileElement buttonFacebook = androidDriver.findElementByXPath("//android.widget.Button" +
                "[@text = 'Continue with Facebook' and @resource-id = 'com.skistar.SKISTAR1:id/facebook_login_button']");

        MobileElement buttonForgotPwd = androidDriver.findElementByXPath("//android.widget.Button" +
                "[@text = 'Forgot password?' and @resource-id = 'com.skistar.SKISTAR1:id/forgot_password_button']");

        MobileElement buttonCreateOne = androidDriver.findElementByXPath("//android.widget.Button" +
                "[@text = 'Create one' and @resource-id = 'com.skistar.SKISTAR1:id/create_account_button']");

        MobileElement textViewNoAccount = androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@text = 'No account?' and @resource-id = 'com.skistar.SKISTAR1:id/no_account_label']");

        // Verify that all the available attributes for 'LoginActivity' are displayed.
        boolean imageButtonXisDisplayed = imageButtonX.isDisplayed();
        System.out.println("ImageButton 'X' is displayed: " + imageButtonXisDisplayed);

        boolean imageViewMySkiStarisDisplayed = imageViewMySkiStar.isDisplayed();
        System.out.println("ImageButton 'X' is displayed: " + imageViewMySkiStarisDisplayed);

    }
}
