package tests.uiTests.verifyOnboarding;

import core.helperClasses.AppLauncher;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TestOnboardingSplashActivity extends AppLauncher {

    AndroidDriver<AndroidElement> androidDriver;

    @BeforeClass
    public void setUp() throws MalformedURLException {
        // Instantiating an object of 'AndroidCapabilities()' class.
        androidDriver = AndroidCapabilities();
    }

    @BeforeMethod
    public void beforeMethod() throws MalformedURLException {
        androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public void afterTests() {
        androidDriver.quit();
    }

    /*
     * This Test Case will perform/verify the following steps
     * 1. Verify Splash Screen is displayed with correct elements i.e. 'Images', 'Texts' and 'Buttons'
     * */

    @Test
    public void TestOnboardingSplashActivityElements() {

        // findElementById: --> Verify that Screen displays the correct Image for 'mountain'
        androidDriver.findElementById("com.skistar.SKISTAR1:id/mountain").isDisplayed();

        // findElementByXPath (using attributes): --> Verify that Launcher Screen displays the correct Image for 'sky'
        androidDriver.findElementByXPath("//android.widget.ImageView[@class = 'android.widget.ImageView' and " +
                "@instance = '1' and @resource-id = 'com.skistar.SKISTAR1:id/sky']").isDisplayed();

        // findElementByXPath (using full hierarchy): --> Verify that Launcher Screen displays the correct Image for 'skier'
        androidDriver.findElementByXPath("//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ImageView[2]").isDisplayed();

        // findElementByAndroidUIAutomator: --> Verify that Launcher Screen displays the correct Image for 'logo'
        // androidDriver.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.skistar.SKISTAR1:id/slope\")").isDisplayed();
        // OR you may use REGEX as given below
        androidDriver.findElementByAndroidUIAutomator("new UiSelector().resourceIdMatches(\".*:id/slope\")").isDisplayed();

        // findElementByAndroidUIAutomator: --> Verify that Launcher Screen displays the correct Text for 'title'
        androidDriver.findElementByAndroidUIAutomator("text(\"Welcome to the World of SkiStar!\")").isDisplayed();

        // Verify that Launcher Screen displays the 'Continue' button
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").isDisplayed();

    }


    /*
     * This Test will perform/verify the following steps
     * 1. Verify Welcome/Splash/Launcher Screen is displayed and has 'Button' (to continue) element is present
     * 2. Perform 'TAP' action on Button.
     * 3. Verify that correct Screen is displayed.
     * */

    @Test
    public void TestOnboardingSplashActivityGestures() {

        // Finding the 'button' (using its Id) that needs to be tapped
        WebElement continueButton = androidDriver.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.skistar.SKISTAR1:id/action_continue\")");

        // Instantiating an object of 'TouchAction()' class, passing 'androidDriver' as argument
        TouchAction touchAction = new TouchAction(androidDriver);

        // TAP: --> Performing TAP action on the 'WebElement' i.e. 'continueButton'
        touchAction.tap(continueButton).perform();

        // OR

        // LongPress: --> Performing LongPress action on the 'WebElement' i.e. 'continueButton' for 2 seconds
        // touchAction.longPress(continueButton, 2).release().perform();
    }
}
