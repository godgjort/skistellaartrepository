package tests.uiTests.verifyOnboarding;

import core.helperClasses.AppLauncher;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestOnboardingActivity extends AppLauncher {

    AndroidDriver<AndroidElement> androidDriver;

    @BeforeMethod
    public void beforeMethod() throws MalformedURLException {
        // Instantiating an object of 'AndroidCapabilities()' class.
        androidDriver = AndroidCapabilities();
        androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public void afterTests() {
        androidDriver.quit();
    }

    @Test
    public void TestOnboardingActivityIntroScreen01() {

        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").click();

        // Verify that 'Skip' button is present and correct Text is displayed for it.
        androidDriver.findElementByAndroidUIAutomator("text(\"Skip\")").isDisplayed();


        // Verify that both 'bottom' and 'top' images are present.
        MobileElement bottomView = androidDriver.findElementById("com.skistar.SKISTAR1:id/bottom");
        MobileElement topView = androidDriver.findElementById("com.skistar.SKISTAR1:id/top");

        // Verify that both 'bottom' and 'top' images are displayed.
        bottomView.isDisplayed();
        topView.isDisplayed();

        // Verify that 'title' TextView is displayed and shows the correct Text.
        String titleText = "Always the latest";
        androidDriver.findElementByXPath("//android.widget.TextView[" +
                "@resource-id = 'com.skistar.SKISTAR1:id/title' and @text =  '" + titleText + "']").isDisplayed();


        // Verify that 'message' TextView is displayed and shows the correct Text.
        String messageText = "Keep track of the latest weather, activities and see what Valle is up to.";
        androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/message' and @text = '" + messageText + "']").isDisplayed();

        // Verify that 'circle_indicator' View is displayed.
        androidDriver.findElementById("com.skistar.SKISTAR1:id/circle_indicator");

        // Verify that 'Continue' TextView is displayed and shows the correct Text and is 'clickable'
        MobileElement continueTextView =  androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/action_next' and @clickable = 'true' and @text = 'Continue']");
        continueTextView.isDisplayed();

    }

    @Test
    public void TestOnboardingActivityIntroScreen02() {

        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();

        // Verify that 'Skip' button is present and correct Text is displayed for it.
        androidDriver.findElementByAndroidUIAutomator("text(\"Skip\")").isDisplayed();

        // Verify that both 'bottom' and 'top' images are displayed.
        androidDriver.findElementById("com.skistar.SKISTAR1:id/bottom").isDisplayed();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/top").isDisplayed();

        // Verify that 'title' TextView is displayed and shows the correct Text.
        String titleText = "Piste maps";
        androidDriver.findElementByXPath("//android.widget.TextView[" +
                "@resource-id = 'com.skistar.SKISTAR1:id/title' and @text =  '" + titleText + "']").isDisplayed();

        // Verify that 'message' TextView is displayed and shows the correct Text.
        String messageText = "The piste maps show lifts, pistes, restaurants, and even the location of your friends!";
        androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/message' and @text = '" + messageText + "']").isDisplayed();

        // Verify that 'circle_indicator' View is displayed.
        androidDriver.findElementById("com.skistar.SKISTAR1:id/circle_indicator");

        // Verify that 'Continue' TextView is displayed and shows the correct Text and is 'clickable'
        MobileElement continueTextView =  androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/action_next' and @clickable = 'true' and @text = 'Continue']");
        continueTextView.isDisplayed();
    }

    @Test
    public void TestOnboardingActivityIntroScreen03() {

        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();

        System.out.println(androidDriver.currentActivity());
        System.out.println(androidDriver.getContext());

        // Verify that 'Skip' button is present and correct Text is displayed for it.
        androidDriver.findElementByAndroidUIAutomator("text(\"Skip\")").isDisplayed();

        // Verify that both 'bottom' and 'top' images are displayed.
        androidDriver.findElementById("com.skistar.SKISTAR1:id/bottom").isDisplayed();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/top").isDisplayed();

        // Verify that 'title' TextView is displayed and shows the correct Text.
        String titleText = "Improve your experience";
        androidDriver.findElementByXPath("//android.widget.TextView[" +
                "@resource-id = 'com.skistar.SKISTAR1:id/title' and @text =  '" + titleText + "']").isDisplayed();

        // Verify that 'message' TextView is displayed and shows the correct Text.
        String messageText = "Log in and see your bookings, register your SkiPass, see statistics and compete in the leaderboards.";
        androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/message' and @text = '" + messageText + "']").isDisplayed();

        // Verify that 'circle_indicator' View is displayed.
        androidDriver.findElementById("com.skistar.SKISTAR1:id/circle_indicator");

        // Verify that 'Continue' TextView is displayed and shows the correct Text and is 'clickable'
        MobileElement continueTextView =  androidDriver.findElementByXPath("//android.widget.TextView" +
                "[@resource-id = 'com.skistar.SKISTAR1:id/action_next' and @clickable = 'true' and @text = 'Log in/create account']");
        continueTextView.isDisplayed();
    }

}
