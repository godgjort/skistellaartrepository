package tests.uiTests.verifyOnboarding;

import core.helperClasses.AppLauncher;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestSelectResort extends AppLauncher {

    AndroidDriver<AndroidElement> androidDriver;

    @BeforeMethod
    public void beforeMethod() throws MalformedURLException {
        // Instantiating an object of 'AndroidCapabilities()' class.
        androidDriver = AndroidCapabilities();
        androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void afterTests() {
        androidDriver.quit();
    }

    @Test
    public void TestSelectResortElements() {

        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_continue").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_next").click();
        androidDriver.findElementById("com.skistar.SKISTAR1:id/action_skip").click();
        System.out.println("Activity Name for SelectResortScreen: " + androidDriver.currentActivity());

        // Verify that 'Select Resort' text is present and correct Text is displayed for it.
        androidDriver.findElementByAndroidUIAutomator("text(\"Select resort\")").isDisplayed();

        // Verify that all the available resorts (as ImageViews) are displayed.
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '0' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '1' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '2' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '3' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '4' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '5' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");
        androidDriver.findElementByXPath("//android.widget.ImageView" +
                "[@instance = '6' and @resource-id = 'com.skistar.SKISTAR1:id/destination_logo' and @enabled = 'true']");

        // Navigating to the Previous screen using Android's Back button as I need to 'Create an Account'
        androidDriver.pressKeyCode(AndroidKeyCode.BACK);
    }
}
