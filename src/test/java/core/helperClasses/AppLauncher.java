package core.helperClasses;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.io.File;
import java.net.URL;

public class AppLauncher {

    public static AndroidDriver<AndroidElement> AndroidCapabilities() throws MalformedURLException {

        // Instantiating a variable to store apk folder location
        File apkFolderLocation = new File("src/test/resources");

        // Instantiating another variable to store 'apk name' and ' apk folder location'
        File apkFileLocation = new File(apkFolderLocation, "com.skistar.SKISTAR1_2019-01-10.apk");

        // Instantiating a URL object that will store the connection URL to Appium Server
        URL domain = new URL("http://127.0.0.1:4723");
        URL connectionUrlToAppiumServer = new URL(domain + "/wd/hub");

        // Creating an instance of 'DesiredCapabilities' class
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

        // Adding Emulator to the desiredCapabilities
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android emulator");
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
        // desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel2XlApi28");
        // desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "e293dc9f");


        // Adding absolute path of the APP to the desiredCapabilities
        desiredCapabilities.setCapability(MobileCapabilityType.APP, apkFileLocation.getAbsolutePath());

        // Setting below Capabilities because Appium was trying to start an incorrect activity i.e.
        // 'se.skistar.android.navigation.MainActivity' or 'com.skistar.SKISTAR1.se.skistar.android.navigation.MainActivity'
        desiredCapabilities.setCapability("autoGrantPermissions",true);
        desiredCapabilities.setCapability("appWaitActivity","se.skistar.android.onboarding.OnboardingSplashActivity");

        // Instantiating an object of AndroidDriver class (it will be used to automate the TestCases on Android OS)
        // We need to pass 2 parameters/arguments to the AndroidDriver() class i.e.
        // 1. connection link to the Appium Server
        // 2. Object of DesiredCapabilities() class
        AndroidDriver<AndroidElement> androidDriver = new AndroidDriver<>(connectionUrlToAppiumServer, desiredCapabilities);

        return androidDriver;

    }
}
